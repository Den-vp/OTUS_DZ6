﻿using System;
using System.Collections.Generic;

namespace DZ6.DB
{
    public interface IGenericTable<TEntity> where TEntity : class
    {
        void Create(TEntity ent);
        void Remove(TEntity ent);
        void Update(TEntity ent);
        List<TEntity> GetList();
        List<TEntity> GetList(Func<TEntity, bool> predicate);
    }
}